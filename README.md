## Multi Lenguage Build-Deploy Pipeline
This pipeline on Gitlab CI/CD build and deploy automatically the images of 3 small _Calculator Apps_ built in different languages (Go, Python, Node) on **Docker Hub**.

All of this is in a gitlab-runner running on a **Google Cloud** virtual machine.

#### Images deplyed on Docker Hub repository
- [Go App](https://hub.docker.com/r/vicenteastorga/golang) 
- [Python App](https://hub.docker.com/r/vicenteastorga/python) 
- [Node App](https://hub.docker.com/r/vicenteastorga/nodejs)


